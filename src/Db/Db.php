<?php
/**
 * Created by PhpStorm.
 * User: Mahim
 * Date: 9/19/2019
 * Time: 9:02 PM
 */

namespace Mahim\Db;

use PDO;
use PDOException;

class Db
{
    private $serverName = "localhost";
    private $dbName = "mahim";
    private $userName = "root";
    private $password = "";

    protected $dbh;

    public function __construct()
 {

     try{

         $dbh = new PDO("mysql:host=$this->serverName;dbname=$this->dbName",$this->userName,$this->password);

         $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         echo "DataBase collection successfull";
     }
     catch (PDOException $e){
         echo "Connection Failed"." ".$e->getMessage();
     }
 }

}