<?php
/**
 * Created by PhpStorm.
 * User: Mahim
 * Date: 9/18/2019
 * Time: 7:42 PM
 */

namespace Mahim\Query\multi;


class Multi
{
    protected $number1 = 10;
    protected  $number2 = 20;

    public function  multi(){
        return $this->number1 * $this->number2;
        self::if_empty(2);
    }

    public function add(){
        $multi = $this->multi();
        return $multi + 10;
    }

    public static function if_empty($value){
        if (empty($value)){
            echo "this is empty";
        }
        else{
            echo "this is not empty";
        }
    }

}