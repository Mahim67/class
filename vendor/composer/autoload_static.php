<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc270238eb0660a6e08ba8e50069e487e
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Mahim\\' => 6,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Mahim\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc270238eb0660a6e08ba8e50069e487e::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc270238eb0660a6e08ba8e50069e487e::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
